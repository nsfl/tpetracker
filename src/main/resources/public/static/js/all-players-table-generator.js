$(document).ready(function () {

    $.fn.dataTable.enum(["Quarterback", "Running Back", "Wide Receiver", "Tight End", "Offensive Line", "Defensive End", "Defensive Tackle", "Linebacker", "Cornerback", "Safety", "Kicker/Punter", "Unknown"]);
    var t = $("#table").DataTable({
        dom: "<'row'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        scrollX: !0,
        paging: !1,
        order: [[6, "desc"]],
        buttons: ["csv", {
            text: "Toggle All", action: function () {
                t.columns().visible(!t.column(0).visible())
            }
        }, {
            text: "Toggle Attributes", action: function () {
                t.columns([12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]).visible(!t.column(12).visible())
            }
        }, {
            extend: 'colvis', text: 'Toggle Columns'
        }, {
            extend: 'collection',
            text: 'Position',
            buttons: [{
                text: "All", action: function () {
                    t.columns(4).search('').draw()
                }
            }, {
                text: "Offense", action: function () {
                    t.columns(4).search("Quarterback|Running|Wide|Tight|Offensive", true).draw()
                }
            }, {
                text: "QB", action: function() {
                    t.columns(4).search("Quarterback").draw()
                }
            }, {
                text: "RB", action: function () {
                    t.columns(4).search("Running Back").draw()
                }
            }, {
                text: "WR", action: function () {
                    t.columns(4).search("Wide Receiver").draw()
                }
            }, {
                text: "TE", action: function () {
                    t.columns(4).search("Tight End").draw()
                }
            }, {
                text: "OL", action: function () {
                    t.columns(4).search("Offensive Line").draw()
                }
            }, {
                text: "Defense", action: function() {
                    t.columns(4).search("Defensive|Linebacker|Cornerback|Safety", true).draw()
                }
            }, {
                text: "DT", action: function () {
                    t.columns(4).search("Defensive Tackle").draw()
                }
            }, {
                text: "DE", action: function () {
                    t.columns(4).search("Defensive End").draw()
                }
            }, {
                text: "LB", action: function () {
                    t.columns(4).search("Linebacker").draw()
                }
            }, {
                text: "CB", action: function () {
                    t.columns(4).search("Cornerback").draw()
                }
            }, {
                text: "S", action: function () {
                    t.columns(4).search("Safety").draw()
                }
            }, {
                text: "K/P", action: function () {
                    t.columns(4).search("Kicker/Punter").draw()
                }
            }],
            autoClose: true
        }],
        data: dataSet,
        columns: [{title: "User"}, {title: "Draft Year"}, {title: "Team"}, {title: "Name"}, {title: "Position"}, {title: "Archetype"}, {title: "Current TPE"}, {title: "TPE Available"},  {title: "Highest TPE"}, {title: "Balance ($M)"}, {title: "Last Updated"}, {title: "Last Seen"}, {title: "Str"}, {title: "Agi"}, {title: "Arm"}, {title: "Int"}, {title: "Thr"}, {title: "Tck"}, {title: "Spd"}, {title: "Hnd"}, {title: "PBlk"}, {title: "RBlk"}, {title: "End"}, {title: "KPow"}, {title: "KAcc"}, {title: "Comp"}, {title: "Purchased Traits"}]
    })
});