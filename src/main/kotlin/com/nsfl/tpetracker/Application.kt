package com.nsfl.tpetracker

import com.fasterxml.jackson.databind.ObjectMapper
import com.nsfl.tpetracker.html.HTMLGenerator
import com.nsfl.tpetracker.model.pasta.CopyPastaRepository
import com.nsfl.tpetracker.model.player.PlayerRepository
import com.nsfl.tpetracker.model.position.Position
import com.nsfl.tpetracker.model.team.Team
import jakarta.servlet.RequestDispatcher
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.http.MediaType
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@SpringBootApplication
@EnableScheduling
class Application {

    private val playerRepository = PlayerRepository()
    private val copyPastaRepository = CopyPastaRepository()
    private val htmlGenerator = HTMLGenerator()
    private var lastUpdateInfo = ""

    init {
        Thread {
            playerRepository.initialise()
            updatePlayers("Initial")
        }.start()
    }

    @Scheduled(cron = "0 0 10 * * MON-FRI")
    fun updatePlayersWeekday() {
        updatePlayers("Weekday")
    }

    @Scheduled(cron = "0 0 1,4,7,10,13,16,19,22 * * SUN,SAT")
    fun updatePlayersWeekend() {
        updatePlayers("Weekend")
    }

    private fun updatePlayers(type: String) {
        Logger.info("$type player update started.")
        val start = System.currentTimeMillis()
        playerRepository.update()
        lastUpdateInfo = "Last Update =>" +
                " Type: $type," +
                " Started At: ${Date(start)}," +
                " Duration: ${System.currentTimeMillis() - start} ms"
        Logger.info("$type player update finished.")
    }

    @RequestMapping("/last_update")
    fun getLastUpdateInformation(): String {
        return lastUpdateInfo
    }

    @RequestMapping("/players_json", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getActivePlayersJson(
            @RequestParam(required = false, defaultValue = "${Int.MIN_VALUE}") gt: Int,
            @RequestParam(required = false, defaultValue = "${Int.MAX_VALUE}") lt: Int
    ) = ObjectMapper().writeValueAsString(playerRepository.getAllPlayers().filter { it.currentTPE in gt..lt })

    @RequestMapping("/retired_players_json")
    fun getRetiredPlayersJson() = ObjectMapper().writeValueAsString(playerRepository.getRetiredPlayers())

    @RequestMapping("/error")
    fun getNextCopyPasta(request: HttpServletRequest) = htmlGenerator.createErrorPage(
            request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE).toString(),
            copyPastaRepository.getNextCopyPasta()
    )

    @RequestMapping("/random_pasta")
    fun getRandomCopyPasta(request: HttpServletRequest) = htmlGenerator.createErrorPage(
            request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE).toString(),
            copyPastaRepository.getRandomCopyPasta()
    )

    @RequestMapping("/")
    fun getIndex() = htmlGenerator.createIndexPage()

    @RequestMapping("/all_players")
    fun getAllPlayers() =
            htmlGenerator.createAllPlayersPage(playerRepository.getAllPlayers())

    @RequestMapping("/player")
    fun getPlayer(@RequestParam playerId: String) =
            playerRepository.getPlayer(playerId).let {
                htmlGenerator.createPlayerPage(it)
            }

    @RequestMapping("/team_stats")
    fun getTeamStats() = htmlGenerator.createTeamStatsPage(
            listOf(
                    Pair(Team.BALTIMORE_HAWKS, playerRepository.getBaltimoreHawksPlayers()),
                    Pair(Team.BLACK_FOREST_BROOD, playerRepository.getBlackForestBroodPlayers()),
                    Pair(Team.OSAKA_KAIJU, playerRepository.getOsakaKaijuPlayers()),
                    Pair(Team.COLORADO_YETI, playerRepository.getColoradoYetiPlayers()),
                    Pair(Team.CAPE_TOWN_CRASH, playerRepository.getCapeTownCrashPlayers()),
                    Pair(Team.SARASOTA_SAILFISH, playerRepository.getSarasotaSailfishPlayers()),
                    Pair(Team.YELLOWKNIFE_WRAITHS, playerRepository.getYellowknifeWraithsPlayers()),
                    Pair(Team.ARIZONA_OUTLAWS, playerRepository.getArizonaOutlawsPlayers()),
                    Pair(Team.AUSTIN_COPPERHEADS, playerRepository.getAustinCopperheadsPlayers()),
                    Pair(Team.HONOLULU_HAHALUA, playerRepository.getHonoluluHahaluaPlayers()),
                    Pair(Team.NEW_ORLEANS_SECOND_LINE, playerRepository.getNewOrleansSecondLinePlayers()),
                    Pair(Team.NEW_YORK_SILVERBACKS, playerRepository.getNewYorkSilverbacksPlayers()),
                    Pair(Team.ORANGE_COUNTY_OTTERS, playerRepository.getOrangeCountyOttersPlayers()),
                    Pair(Team.SAN_JOSE_SABERCATS, playerRepository.getSanJoseSabercatsPlayers())
            ),
            listOf(
                    Pair(Team.BONDI_BEACH_BUCCANEERS, playerRepository.getBondiBeachBuccaneersPlayers()),
                    Pair(Team.KANSAS_CITY_COYOTES, playerRepository.getKansasCityCoyotesPlayers()),
                    Pair(Team.PORTLAND_PYTHONS, playerRepository.getPortlandPythonsPlayers()),
                    Pair(Team.NORFOLK_SEAWOLVES, playerRepository.getNorfolkSeawolvesPlayers()),
                    Pair(Team.MINNESOTA_GREY_DUCKS, playerRepository.getMinnesotaGreyDucksPlayers()),
                    Pair(Team.TIJUANA_LUCHADORES, playerRepository.getTijuanaLuchadoresPlayers()),
                    Pair(Team.DALLAS_BIRDDOGS, playerRepository.getDallasBirddogsPlayers()),
                    Pair(Team.LONDON_ROYALS, playerRepository.getLondonRoyalsPlayers())
            )
    )

    @RequestMapping("/position_stats")
    fun getPositionStats() = htmlGenerator.createPositionStatsPage(
            listOf(
                    Pair(Position.QB, playerRepository.getQBPlayers()),
                    Pair(Position.RB, playerRepository.getRBPlayers()),
                    Pair(Position.WR, playerRepository.getWRPlayers()),
                    Pair(Position.TE, playerRepository.getTEPlayers()),
                    Pair(Position.OL, playerRepository.getOLPlayers()),
                    Pair(Position.DE, playerRepository.getDEPlayers()),
                    Pair(Position.DT, playerRepository.getDTPlayers()),
                    Pair(Position.LB, playerRepository.getLBPlayers()),
                    Pair(Position.CB, playerRepository.getCBPlayers()),
                    Pair(Position.S, playerRepository.getSPlayers()),
                    Pair(Position.KP, playerRepository.getKPPlayers())
            )
    )

    @RequestMapping("/baltimore_hawks")
    fun getBaltimoreHawksPlayers() = htmlGenerator.createIndividualTeamPage(
            playerRepository.getBaltimoreHawksPlayers(),
            Team.BALTIMORE_HAWKS
    )

     @RequestMapping("/black_forest_brood")
    fun getBlackForestBroodPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getBlackForestBroodPlayers(),
            Team.BLACK_FOREST_BROOD
    )

    @RequestMapping("/chicago_butchers")
    fun getChicagoButchersPlayers(response: HttpServletResponse) =
        response.sendRedirect("/osaka_kaiju")

    @RequestMapping("/osaka_kaiju")
    fun getOsakaKaijuPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getOsakaKaijuPlayers(),
            Team.OSAKA_KAIJU
    )

    @RequestMapping("/colorado_yeti")
    fun getColoradoYetiPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getColoradoYetiPlayers(),
            Team.COLORADO_YETI
    )

    @RequestMapping("/cape_town_crash")
    fun getCapeTownCrashPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getCapeTownCrashPlayers(),
            Team.CAPE_TOWN_CRASH
    )

    @RequestMapping("/yellowknife_wraiths")
    fun getYellowknifeWraithsPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getYellowknifeWraithsPlayers(),
            Team.YELLOWKNIFE_WRAITHS
    )

    @RequestMapping("/arizona_outlaws")
    fun getArizonaOutlawsPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getArizonaOutlawsPlayers(),
            Team.ARIZONA_OUTLAWS
    )

    @RequestMapping("/austin_copperheads")
    fun getAustinCopperheadsPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getAustinCopperheadsPlayers(),
            Team.AUSTIN_COPPERHEADS
    )

    @RequestMapping("/new_orleans_second_line")
    fun getNewOrleansSecondLinePlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getNewOrleansSecondLinePlayers(),
            Team.NEW_ORLEANS_SECOND_LINE
    )

    @RequestMapping("/new_york_silverbacks")
    fun getNewYorkSilverbacksPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getNewYorkSilverbacksPlayers(),
            Team.NEW_YORK_SILVERBACKS
    )

    @RequestMapping("/orange_county_otters")
    fun getOrangeCountyOttersPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getOrangeCountyOttersPlayers(),
            Team.ORANGE_COUNTY_OTTERS
    )

    @RequestMapping("/san_jose_sabercats")
    fun getSanJoseSaberCatsPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getSanJoseSabercatsPlayers(),
            Team.SAN_JOSE_SABERCATS
    )

    @RequestMapping("/honolulu_hahalua")
    fun getHonoluluHahaluaPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getHonoluluHahaluaPlayers(),
            Team.HONOLULU_HAHALUA
    )

    @RequestMapping("/sarasota_sailfish")
    fun getSarasotaSailfishPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getSarasotaSailfishPlayers(),
            Team.SARASOTA_SAILFISH
    )

    @RequestMapping("/bondi_beach_buccaneers")
    fun getBondiBeachBuccaneersPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getBondiBeachBuccaneersPlayers(),
            Team.BONDI_BEACH_BUCCANEERS
    )

    @RequestMapping("/kansas_city_coyotes")
    fun getKansasCityCoyotesPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getKansasCityCoyotesPlayers(),
            Team.KANSAS_CITY_COYOTES
    )

    @RequestMapping("/portland_pythons")
    fun getPortlandPythonsPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getPortlandPythonsPlayers(),
            Team.PORTLAND_PYTHONS
    )

    @RequestMapping("/norfolk_seawolves")
    fun getNorfolkSeawolvesPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getNorfolkSeawolvesPlayers(),
            Team.NORFOLK_SEAWOLVES
    )

    @RequestMapping("/minnesota_grey_ducks")
    fun getMinnesotaGreyDucksPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getMinnesotaGreyDucksPlayers(),
            Team.MINNESOTA_GREY_DUCKS
    )

    @RequestMapping("/tijuana_luchadores")
    fun getTijuanaLuchadoresPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getTijuanaLuchadoresPlayers(),
            Team.TIJUANA_LUCHADORES
    )

    @RequestMapping("/dallas_birddogs")
    fun getDallasBirddogsPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getDallasBirddogsPlayers(),
            Team.DALLAS_BIRDDOGS
    )

    @RequestMapping("/london_royals")
    fun getLondonRoyalsPlayers() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getLondonRoyalsPlayers(),
            Team.LONDON_ROYALS
    )

    @RequestMapping("/free_agents")
    fun getFreeAgents() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getFreeAgents(),
            Team.FREE_AGENTS
    )

    @RequestMapping("/qb_prospects")
    fun getQBProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getQBProspects(),
            Team.QB_PROSPECTS
    )

    @RequestMapping("/rb_prospects")
    fun getRBProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getRBProspects(),
            Team.RB_PROSPECTS
    )

    @RequestMapping("/wr_prospects")
    fun getWRProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getWRProspects(),
            Team.WR_PROSPECTS
    )

    @RequestMapping("/te_prospects")
    fun getTEProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getTEProspects(),
            Team.TE_PROSPECTS
    )

    @RequestMapping("/ol_prospects")
    fun getOLProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getOLProspects(),
            Team.OL_PROSPECTS
    )

    @RequestMapping("/de_prospects")
    fun getDEProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getDEProspects(),
            Team.DE_PROSPECTS
    )

    @RequestMapping("/dt_prospects")
    fun getDTProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getDTProspects(),
            Team.DT_PROSPECTS
    )

    @RequestMapping("/lb_prospects")
    fun getLBProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getLBProspects(),
            Team.LB_PROSPECTS
    )

    @RequestMapping("/cb_prospects")
    fun getCBProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getCBProspects(),
            Team.CB_PROSPECTS
    )

    @RequestMapping("/s_prospects")
    fun getSProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getSProspects(),
            Team.S_PROSPECTS
    )

    @RequestMapping("/kp_prospects")
    fun getKPProspects() = htmlGenerator.createIndividualTeamPage(
                    playerRepository.getKPProspects(),
            Team.KP_PROSPECTS
    )

    @RequestMapping("/retired_players")
    fun getRetiredPlayers() =
            htmlGenerator.createRetiredPlayersPage(playerRepository.getRetiredPlayers())

    @Controller
    class CustomErrorController : ErrorController {
        fun getErrorPath() = "/error"
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
