package com.nsfl.tpetracker.model.player

import com.nsfl.tpetracker.Logger
import com.nsfl.tpetracker.model.position.Position
import com.nsfl.tpetracker.model.team.Team
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.regex.Pattern
import kotlin.math.absoluteValue
import kotlin.math.roundToInt


class MyBBPlayerParser {

    private val inputDateFormat = SimpleDateFormat("MM-dd-yyyy',' hh:mm")
    private val lastSeenDateFormat = SimpleDateFormat("yyyy/MM/dd")

    private val bankSheetCsvUrl = "https://docs.google.com/spreadsheets/d/1RYS8I7xCcq-HABK_tiGsaJ5saq6KNkhFJb53C11XqpU/pub?gid=1800743862&output=csv"
    private lateinit var bankSheetCsv: String

    private fun getBankSheet(): String {
        return connect(bankSheetCsvUrl).body().text()
    }

    fun parseActivePlayers() = ArrayList<ParsedPlayer>().apply {
        bankSheetCsv = getBankSheet()
        Team.values().forEach { addAll(parsePlayers(it)) }
    }

    fun parsePlayers(team: Team): List<ParsedPlayer> {

        val playerList = ArrayList<ParsedPlayer>()
        val documentList = ArrayList<Document>()

        val firstDocument = connect("https://forums.sim-football.com/forumdisplay.php?fid=${team.id}")
        documentList.add(firstDocument)

        val pageCount = parsePageCount(firstDocument.body().toString())
        if(pageCount>1){
            for (i in 2..pageCount) {
                println("Adding page $i")
                documentList.add(connect("https://forums.sim-football.com/forumdisplay.php?fid=${team.id}&page=${i}"))
            }
        }

        documentList.forEach { document ->
            document.body().getElementsByClass("inline_row")?.forEach { row -> row.allElements
                    ?.filter { element ->
                        element.toString().let {
                            it.startsWith("<td class=\"trow")
                                    && !it.contains("Moved:")
                                    && it.contains(">(S")
                        }
                    }?.forEach { element ->
                        element.toString().let {

                            val playerInfo = parsePlayerInfo(it).split("?")

                            if (playerInfo.size == 3) {

                                try{
                                    val playerId = parsePlayerID(it)
                                    val playerPostDoc = connect("https://forums.sim-football.com/showthread.php?tid=${playerId}")

                                    try {
                                        val playerPost = playerPostDoc.body()
                                                            .getElementsByClass("post")[0]

                                        val author = playerPost.getElementsByClass("post_author")[0]
                                        val user = author.getElementsByClass("largetext").text()
                                        val playerLink = author.getElementsByAttribute("href")[0].attr("href")
                                        val playerProfileDoc = connect(playerLink)
                                        val playerProfile = playerProfileDoc.body().getElementsByClass("trow2")

                                        val attributes = playerPost.getElementsByClass("post_body").toString()

                                        playerList.add(
                                                ParsedPlayer(
                                                        playerId,
                                                        user.replace("'", "’"),
                                                        playerInfo[1].trim(),
                                                        team,
                                                        Position.fromAbbreviation(playerInfo[2].trim()),
                                                        playerInfo[0].trim().let { info ->
                                                            if (info.length == 2) {
                                                                "S0${info.substring(1)}"
                                                            } else {
                                                                info
                                                            }
                                                        },
                                                        parsePlayerTPE(it),
                                                        parsePlayerAttribute(attributes, "tpe available:"),
                                                        parsePlayerAttribute(attributes, "strength:"),
                                                        parsePlayerAttribute(attributes, "agility:"),
                                                        parsePlayerAttribute(attributes, "arm:"),
                                                        parsePlayerAttribute(attributes, "intelligence:"),
                                                        parsePlayerAttribute(attributes, "throwing accuracy:"),
                                                        parsePlayerAttribute(attributes, "tackling:"),
                                                        parsePlayerAttribute(attributes, "speed:"),
                                                        parsePlayerAttribute(attributes, "hands:"),
                                                        parsePlayerAttribute(attributes, "pass blocking:"),
                                                        parsePlayerAttribute(attributes, "run blocking:"),
                                                        parsePlayerAttribute(attributes, "endurance:"),
                                                        parsePlayerAttribute(attributes, "kick power:"),
                                                        parsePlayerAttribute(attributes, "kick accuracy:"),
                                                        parseLastSeen(playerProfile),
                                                        parsePlayerAttribute(attributes, "competitiveness:"),
                                                        parseTraits(attributes),
                                                        parseField(attributes, "Archetype:"),
                                                        parseBankBalance(user),
                                                        parseField(attributes, "birthplace:")
                                                )
                                        )
                                } catch (exception: Exception) {
                                    Logger.error("Issue parsing player with Player ID: $playerId ! $exception")
                                }

                        } catch (exception: Exception){
                                    Logger.error("Issue parsing thread table row! $exception")
                                }
                            }
                    }
                  }
            }
        }

        return playerList
    }

    private fun parsePageCount(document: String): Int {
        return try {
            val startIndex = document.indexOf("Pages (")
            val endIndex = document.indexOf(")", startIndex)
            document.substring(startIndex, endIndex)
                    .replace(Pattern.compile("[^0-9.]").toRegex(), "")
                    .toInt()
        } catch (exception: Exception) {
            1
        }
    }

    private fun parsePlayerInfo(elementString: String): String {
        val startIndex = elementString.indexOf(">(S") + 1
        val endIndex = elementString.indexOf("<", startIndex)
        return elementString.substring(startIndex, endIndex)
                .replace("–", "-")
                .replace("(", "")
                .replace(")", "")
                .replace("'", "’")
                .replaceFirst("-", "?")
                .reversed()
                .replaceFirst("-", "?")
                .reversed()
    }

    private fun parsePlayerTPE(elementString: String): Int {
        return try {
            val startIndex = elementString.indexOf("TPE:")
            val endIndex = elementString.indexOf("<", startIndex)

            elementString.substring(startIndex, endIndex)
                    .replace(Pattern.compile("[^0-9.]").toRegex(), "")
                    .toInt()
        } catch (exception: Exception) {
            50
        }
    }

    private fun parsePlayerID(elementString: String): String {

            val regex = """href="showthread.php\?tid=([0-9]+)">\(""".toRegex()
            val matchResult = regex.find(elementString) ?: throw Exception()
            return matchResult.groupValues[1]

    }

    fun parseUserName(playerID: String): String{
        try {
            val con_user = connect("https://forums.sim-football.com/showthread.php?tid=$playerID") ?: throw Exception()
            val user = con_user
                    .body()
                    .getElementsByClass("post")[0]
                    .getElementsByClass("post_author")[0]
                    .getElementsByClass("largetext")
                    .text()

            return user.replace("'", "’")
        } catch (exception: Exception) {
            return playerID
        }
    }

    private fun parseField(post: String, searchString: String): String {
        return try {
            var archetype = ""
            var started = false
            var finished = false

            val cleanedPost = Jsoup.parse(post).wholeText()
                    .replace("*", "")
                    .replace("nbsp", "")

            var index = cleanedPost.indexOf(searchString, ignoreCase = true)

            if (index == -1)
                return "Unknown"

            while (!finished) {

                val char = cleanedPost[index + searchString.length]

                if (!started && !char.isLetter()) {

                } else if (char.isLetter() || char.compareTo(45.toChar()) == 0 ||
                        char.isWhitespace() && char.compareTo(10.toChar()) != 0
                        || char.compareTo(44.toChar()) == 0) {
                    archetype+=char
                    started = true
                } else if (started) {
                    finished = true
                }

                index++

            }
            archetype = archetype.trim()

            if (archetype.isEmpty()) {
                return "Unknown"
            }

            archetype

        } catch (exception: Exception) {
            "Unknown"
        }
    }



    private fun parseTraits(post: String): MutableList<String> {
        return try {
            var listOfTraits = mutableListOf<String>()
            var foundTrait = ""
            var started = false
            var finished = false
            var traitEnd = false
            var searchString = "Purchased Traits:"

            val cleanedPost = Jsoup.parse(post).wholeText()
                    .replace("*", "")
                    .replace("nbsp", "")

            var index = cleanedPost.indexOf(searchString, ignoreCase = true)
            var indexOfAvailableTraits = cleanedPost.indexOf("Available Traits", ignoreCase = true)

            if (index + searchString.length >= indexOfAvailableTraits) {
                indexOfAvailableTraits = -1
            }


            if (index == -1)
                return mutableListOf()

            while (!finished) {

                if(index + searchString.length >= cleanedPost.length ||
                        (indexOfAvailableTraits != -1 && index + searchString.length >= indexOfAvailableTraits)) {
                    break
                }

                val char = cleanedPost[index + searchString.length]

                if (traitEnd) {
                    if (char.compareTo(10.toChar()) == 0) {
                        traitEnd = false
                    }
                } else if (!started && !char.isLetter()) {

                } else if (char.isLetter() || char.compareTo(45.toChar()) == 0 ||
                        char.isWhitespace() && char.compareTo(10.toChar()) != 0) {
                        foundTrait += char
                        started = true
                } else if (started) {
                    listOfTraits.add(foundTrait.trimEnd())
                    started = false
                    foundTrait = ""
                    traitEnd = true

                }

                index++

            }

            listOfTraits

        } catch (exception: Exception) {
            mutableListOf()
        }
    }

    private fun parsePlayerAttribute(post: String, attributeName: String): Int {
        return try {

            var attribute = ""
            var started = false
            var finished = false
            var index = post.indexOf(attributeName, ignoreCase = true)

            while (!finished) {

                val char = post[index]

                if (char.isDigit()) {
                    started = true
                    attribute += char
                } else if (started) {
                    finished = true
                }

                index++
            }
                attribute.toInt()

        } catch (exception: Exception) {
            -999
        }
    }

    private fun parseLastSeen(profile_elements: Elements): String {

        val visit_elements = profile_elements.filter { element ->
            element.toString().contains("Last Visit:")
        }

        val lastSeenString = visit_elements[0].nextElementSibling().toString()

        if (lastSeenString.contains("(Hidden)"))
            return lastSeenDateFormat.format(inputDateFormat.parse("01-01-1900, 00:00"))

        val regex = "([0-9]{2}-[0-9]{2}-[0-9]{4}).*?([0-9]{2}:[0-9]{2} [PA]M)".toRegex()
        val matchResult = regex.find(lastSeenString) ?: throw Exception()
        return lastSeenDateFormat.format(inputDateFormat.parse(matchResult.groupValues[1] + ", " + matchResult.groupValues[2]))

    }
    private fun parseBankBalance(username: String): Double {
        return try {
            val usernameIndex = bankSheetCsv.indexOf(username, ignoreCase = true)
            val moneyStartIndex = bankSheetCsv.indexOf("\$", usernameIndex)
            val moneyEndIndex = bankSheetCsv.indexOf("\"", moneyStartIndex)

            var x = bankSheetCsv.substring(moneyStartIndex + 1, moneyEndIndex).replace(",", "").toDouble()

            var bankBalance = truncateNumber(x)

            if (bankSheetCsv[moneyStartIndex - 1] == 45.toChar())
                bankBalance *= -1

            bankBalance
        }
        catch (exception: Exception){
            return -999.0
        }
    }

    fun truncateNumber(doubleNumber: Double): Double {
        val thousand = 1000L
        val million = 1000000L
        val billion = 1000000000L
        val trillion = 1000000000000L
        val number = doubleNumber.roundToInt().toDouble()
        if (number.absoluteValue < million) {
            val thousandNumber = number.absoluteValue.toString().reversed().substring(3).reversed()
            if (thousandNumber.length == 5)
                return "0.${thousandNumber.substring(0,2)}".toDouble()
            else if (thousandNumber.length == 4)
                return "0.0${thousandNumber.substring(0,1)}".toDouble()
            else if (thousandNumber.length == 3)
                return 0.0
        } else if (number.toLong().absoluteValue in million until billion) {
            val fraction = calculateFraction(number, million)
            return fraction
        }
        return number
    }

    private fun calculateFraction(number: Double, divisor: Long): Double {
        val calced = number / divisor

        return BigDecimal(calced).setScale(2, RoundingMode.HALF_EVEN).toDouble()
    }

    private fun connect(url: String): Document {
        while (true) {
            try {
                return Jsoup.connect(url)
                        .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0")
                        .get()
            } catch (exception: Exception) {
                Logger.error("Jsoup.connect failed. $exception")
            }
        }
    }

    data class ParsedPlayer(
            val id: String,
            val user: String,
            val name: String,
            val team: Team,
            val position: Position,
            val draftYear: String,
            val tpe: Int,
            val tpeAvailable: Int,
            val strength: Int,
            val agility: Int,
            val arm: Int,
            val intelligence: Int,
            val throwingAccuracy: Int,
            val tackling: Int,
            val speed: Int,
            val hands: Int,
            val passBlocking: Int,
            val runBlocking: Int,
            val endurance: Int,
            val kickPower: Int,
            val kickAccuracy: Int,
            val lastSeen: String,
            val competitiveness: Int,
            val traits: MutableList<String>,
            val archetype: String,
            val bankBalance: Double,
            val birthplace: String
    )
}
