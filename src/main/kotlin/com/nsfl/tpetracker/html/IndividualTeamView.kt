package com.nsfl.tpetracker.html

import com.nsfl.tpetracker.model.player.ActivePlayer
import com.nsfl.tpetracker.model.team.Team
import kotlinx.html.*

fun DIV.individualTeamView(teamPlayers: List<ActivePlayer>, team: Team) = tablePage(team.full, "all-players-table-generator.js") {

    array(
            teamPlayers.map {
                array(
                        "<a href=\"https://forums.sim-football.com/showthread.php?tid=${it.id}\">${it.user}</a>",
                        it.draftYear,
                        "<a href=\"${it.team.url}\">${it.team.full}</a>",
                        "<a href=\"/player?playerId=${it.id}\">${it.name}</a>",
                        it.position.full,
                        it.archetype,
                        it.currentTPE,
                        it.tpeAvailable,
                        it.highestTPE,
                        it.bankBalance,
                        it.lastUpdated,
                        it.lastSeen,
                        it.strength,
                        it.agility,
                        it.arm,
                        it.intelligence,
                        it.throwingAccuracy,
                        it.tackling,
                        it.speed,
                        it.hands,
                        it.passBlocking,
                        it.runBlocking,
                        it.endurance,
                        it.kickPower,
                        it.kickAccuracy,
                        it.competitiveness,
                        it.traits.joinToString()
                )
            }
    )

}
