package com.nsfl.tpetracker.html

import com.nsfl.tpetracker.model.player.ActivePlayer
import com.nsfl.tpetracker.model.player.Player
import com.nsfl.tpetracker.model.player.RetiredPlayer
import com.nsfl.tpetracker.model.position.Position
import com.nsfl.tpetracker.model.team.Team
import kotlinx.html.TagConsumer
import kotlinx.html.html
import kotlinx.html.stream.appendHTML
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HTMLGenerator {

    fun createIndexPage(): String = buildString {
        appendHTML().apply {
            html {
                indexView()
            }
        }
    }

    fun createAllPlayersPage(playerList: List<ActivePlayer>) = buildString {
        appendHTML().apply {
            html {
                navbarTemplate("All Players") {
                    allPlayersView(playerList)
                }
            }
        }
    }

    fun createPlayerPage(player: Player) = buildString {
        appendHTML().apply {
            html {
                navbarTemplate(player.name) {
                    individualPlayerView(player)
                }
            }
        }
    }

    fun createTeamStatsPage(nsflList: List<Pair<Team, List<ActivePlayer>>>, dsflList: List<Pair<Team, List<ActivePlayer>>>) = buildString {
        appendHTML().apply {
            html {
                navbarTemplate("Team Stats") {
                    teamStatsView(nsflList, dsflList)
                }
            }
        }
    }

    fun createPositionStatsPage(positionList: List<Pair<Position, List<ActivePlayer>>>) = buildString {
        appendHTML().apply {
            html {
                navbarTemplate("Position Stats") {
                    positionStatsView(positionList)
                }
            }
        }
    }

    fun createIndividualTeamPage(playerList: List<ActivePlayer>, team: Team) = buildString {
        appendHTML().apply {
            html {
                navbarTemplate(team.full) {
                    individualTeamView(playerList,team)
                }
            }
        }
    }

    fun createRetiredPlayersPage(playerList: List<RetiredPlayer>) = buildString {
        appendHTML().apply {
            html {
                navbarTemplate("Retired Players") {
                    retiredPlayersView(playerList)
                }
            }
        }
    }

    fun createErrorPage(error: String, copyPasta: String) = buildString {
        appendHTML().apply {
            html {
                navbarTemplate("Send Help - 404") {
                    errorPage(error, copyPasta)
                }
            }
        }
    }

}
