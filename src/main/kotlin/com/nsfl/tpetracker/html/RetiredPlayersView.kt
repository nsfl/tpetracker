package com.nsfl.tpetracker.html


import com.nsfl.tpetracker.model.player.RetiredPlayer
import kotlinx.html.*

fun DIV.retiredPlayersView(allPlayers: List<RetiredPlayer>) = tablePage("All Players", "retired-players-table-generator.js") {

    array(
            allPlayers.map {
                array(
                        "<a href=\"https://forums.sim-football.com/showthread.php?tid=${it.id}\">${it.user}</a>",
                        it.draftYear,
                        "<a href=\"${it.team.url}\">${it.team.full}</a>",
                        "<a href=\"/player?playerId=${it.id}\">${it.name}</a>",
                        it.position.full,
                        it.currentTPE,
                        it.highestTPE,
                        it.lastUpdated
                )
            }
    )

}
