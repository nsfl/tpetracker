package com.nsfl.tpetracker.html

import kotlinx.html.*


fun DIV.errorPage(error: String, copyPasta: String) {
    container{
        p(classes = "errorMessage") {
            style = "font-size: 100px; text-align: center;"
            + error
        }
        br{}
        br{}
        p(classes = "pasta") {
            style = "text-align: center;"
            + copyPasta
        }
        br{}
        br{}
        br{}
        a(classes = "pastaButton") {
            style = "text-align: center; display: block; font-size: 20px;"
            href = "/random_pasta"
            + "Hit me!"
        }
    }
}
